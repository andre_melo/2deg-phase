# 2DEG phase

Minimal simulation of arXiv:2103.05651

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kwant-project.org%2Fandre_melo%2F2deg-phase.git/HEAD?filepath=2deg.ipynb)
